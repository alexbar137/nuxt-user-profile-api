const express = require('express')
const bodyParser = require('body-parser')
const { promisify } = require('util')
const dotenv = require('dotenv')
const apiRoutes = require('./routes/api-routes')
const cors = require('cors')

const app = express()
app.use(cors())
dotenv.config()
const db = require('./database/db')

app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', apiRoutes);

const startServer = async () => {
  const port = process.env.SERVER_PORT || 3000
  await promisify(app.listen).bind(app)(port)
  console.log(`Listening on port ${port}`)
}

startServer()
